import styles from './pageTitle.module.css'

const PageTitle = () => <h1 className={styles.title}>PWA Workshop!</h1>

export default PageTitle
