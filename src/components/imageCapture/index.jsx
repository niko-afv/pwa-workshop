'use client'
import { useRef, useState, useEffect } from 'react'
import Button from '@mui/material/Button'
import CameraAltIcon from '@mui/icons-material/CameraAlt'
import CloseIcon from '@mui/icons-material/Close'
import CameraswitchIcon from '@mui/icons-material/Cameraswitch'

import styles from './imageCapture.module.css'
import Box from '@mui/material/Box'

const ImageCapture = () => {
  const videoRef = useRef(null)
  const photoRef = useRef(null)

  const [videoStarted, setVideoStarted] = useState(false)
  const [facingMode, setFacingMode] = useState('user')

  const switchCamera = () => {
    stopCamera()
    if (facingMode === 'user') {
      setFacingMode('environment')
    }
    if (facingMode === 'environment') {
      setFacingMode('user')
    }
    startCamera()
  }

  const startCamera = () => {
    setVideoStarted(true)
    navigator.mediaDevices
      .getUserMedia({
        video: {
          width: { min: 640, ideal: 1920, max: 1920 },
          height: { min: 400, ideal: 1080 },
          aspectRatio: 1.777777778,
          frameRate: { max: 30 },
          facingMode,
        },
      })
      .then((stream) => {
        let video = videoRef.current
        video.srcObject = stream
        video.play()
      })
      .catch((err) => {
        console.log(console.error(err))
      })
  }

  const stopCamera = () => {
    let photo = photoRef.current
    let ctx = photo.getContext('2d')

    const video = videoRef.current
    const stream = video.srcObject

    const tracks = stream.getTracks()
    tracks.forEach((track) => {
      track.stop()
    })
    video.srcObject = null

    ctx.clearRect(0, 0, 0, 0)
    setVideoStarted(false)
  }

  const capture = () => {
    let video = videoRef.current
    let photo = photoRef.current

    photo.width = video.videoWidth
    photo.height = video.videoHeight

    let ctx = photo.getContext('2d')
    ctx.drawImage(video, 0, 0, photo.width, photo.height)
  }

  return (
    <section className={styles.container}>
      {videoStarted === true && (
        <div className={styles.camera}>
          <video className={styles.videoFrame} ref={videoRef}></video>

          <Button
            className={styles.captureButton}
            onClick={capture}
            variant={'contained'}
          >
            <CameraAltIcon />
          </Button>

          <Button
            className={styles.switchButton}
            variant={'contained'}
            onClick={switchCamera}
          >
            <CameraswitchIcon />
          </Button>
        </div>
      )}
      {videoStarted === true ? (
        <>
          <Button
            variant={'outlined'}
            onClick={stopCamera}
            startIcon={<CloseIcon />}
          >
            Detener Cámara
          </Button>

          <div className={styles.result}>
            <canvas className={styles.canvas} ref={photoRef}></canvas>
          </div>
        </>
      ) : (
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'column',
          }}
        >
          <Button
            sx={{ width: '100%' }}
            onClick={startCamera}
            variant={'outlined'}
            startIcon={<CameraAltIcon />}
          >
            Iniciar Cámara
          </Button>
        </Box>
      )}
    </section>
  )
}

export default ImageCapture
