'use client'
import { useState, useEffect, useRef } from 'react'
import Button from '@mui/material/Button'
import Box from '@mui/material/Box'
import LocationOnIcon from '@mui/icons-material/LocationOn'
import CloseIcon from '@mui/icons-material/Close'
import CircularProgress from '@mui/material/CircularProgress'
import { Loader } from '@googlemaps/js-api-loader'

import config from '../../config'
import styles from './locationCapture.module.css'

const LocationCapture = () => {
  const googlemap = useRef(null)
  const [gettingLocation, setGettingLocation] = useState(false)
  const [locationTaken, setLocationTaken] = useState(false)
  const [myLatLng, setMyLatLng] = useState({})
  const [myAccuracy, setMyAccuracy] = useState(null)

  const capture = () => {
    setGettingLocation(true)
    if (!'geolocation' in navigator) {
      alert('el browser no acepta ubicación')
    }

    const onSuccess = (location) => {
      const { coords } = location
      const { latitude, longitude, accuracy } = coords
      setMyLatLng({
        lat: latitude,
        lng: longitude,
      })
      setMyAccuracy(accuracy)
      console.log('Ubicación obtenida', coords)
      setGettingLocation(false)
      setLocationTaken(true)
    }

    const onError = () => console.log('Hubo un error')

    const options = {
      enableHighAccuracy: true,
      maximumAge: 0,
      timeout: 15000,
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError, options)
  }

  const closeMap = () => {
    setMyLatLng({
      lat: 0,
      lng: 0,
    })
    setLocationTaken(false)
  }

  useEffect(() => {
    console.log(myLatLng)
    if (locationTaken === true) {
      const loader = new Loader({
        apiKey: config.google_maps_api_key,
        version: 'weekly',
      })
      let map
      loader.load().then(() => {
        const google = window.google

        map = new google.maps.Map(googlemap.current, {
          center: myLatLng,
          zoom: 17,
          fullscreenControl: false,
          mapTypeControl: false,
          streetViewControl: false,
          zoomControl: true,
        })
        new google.maps.Marker({
          position: myLatLng,
          map,
          title: 'Hello World!',
        })
      })
    }
  }, [locationTaken])

  return (
    <Box className={styles.container}>
      {locationTaken === true ? (
        <>
          <Button
            sx={{ width: '100%' }}
            variant="outlined"
            disabled={gettingLocation}
            startIcon={<CloseIcon />}
            onClick={closeMap}
          >
            Cerrar Mapa
          </Button>
          <div>
            <p>Latitud: {myLatLng.lat}</p>
            <p>Longitud: {myLatLng.lng}</p>
            <p>Accuracy: {myAccuracy}</p>
          </div>
          <div className={styles.map} ref={googlemap} />
        </>
      ) : (
        <Box className={styles.buttonContainer}>
          <Button
            sx={{ width: '100%' }}
            variant="outlined"
            disabled={gettingLocation}
            startIcon={<LocationOnIcon />}
            onClick={capture}
          >
            Capturar Ubicación
          </Button>
          {gettingLocation && (
            <CircularProgress
              size={24}
              sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                marginTop: '-12px',
                marginLeft: '-12px',
              }}
            />
          )}
        </Box>
      )}
    </Box>
  )
}

export default LocationCapture
