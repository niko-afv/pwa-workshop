import PageTitle from '../components/pageTitle'
import ImageCapture from '../components/imageCapture'
import LocationCapture from '../components/locationCapture'

// either Static metadata
export const metadata = {
  title: 'PWA Workshop',
  description: 'PWA demo for Workshop',
}

const Page = () => {
  return (
    <>
      <PageTitle />
      <ImageCapture />

      <LocationCapture />
    </>
  )
}

export default Page
